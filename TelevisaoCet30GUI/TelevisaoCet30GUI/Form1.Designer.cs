﻿namespace TelevisaoCet30GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelStatus = new System.Windows.Forms.Label();
            this.ButtonOnOff = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelCanal = new System.Windows.Forms.Label();
            this.ButtonAumentaCanal = new System.Windows.Forms.Button();
            this.ButtonDiminuiCanal = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TrackBarVolume = new System.Windows.Forms.TrackBar();
            this.labelVolume = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.Location = new System.Drawing.Point(13, 200);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(84, 20);
            this.labelStatus.TabIndex = 0;
            this.labelStatus.Text = "StatusTV";
            // 
            // ButtonOnOff
            // 
            this.ButtonOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonOnOff.Location = new System.Drawing.Point(300, 175);
            this.ButtonOnOff.Name = "ButtonOnOff";
            this.ButtonOnOff.Size = new System.Drawing.Size(75, 66);
            this.ButtonOnOff.TabIndex = 1;
            this.ButtonOnOff.Text = "ON";
            this.ButtonOnOff.UseVisualStyleBackColor = true;
            this.ButtonOnOff.Click += new System.EventHandler(this.ButtonOnOff_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelCanal);
            this.groupBox1.Controls.Add(this.ButtonAumentaCanal);
            this.groupBox1.Controls.Add(this.ButtonDiminuiCanal);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(174, 139);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Canais";
            // 
            // labelCanal
            // 
            this.labelCanal.AutoSize = true;
            this.labelCanal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCanal.Location = new System.Drawing.Point(66, 46);
            this.labelCanal.Name = "labelCanal";
            this.labelCanal.Size = new System.Drawing.Size(34, 31);
            this.labelCanal.TabIndex = 2;
            this.labelCanal.Text = "--";
            // 
            // ButtonAumentaCanal
            // 
            this.ButtonAumentaCanal.Enabled = false;
            this.ButtonAumentaCanal.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAumentaCanal.Location = new System.Drawing.Point(106, 19);
            this.ButtonAumentaCanal.Name = "ButtonAumentaCanal";
            this.ButtonAumentaCanal.Size = new System.Drawing.Size(57, 58);
            this.ButtonAumentaCanal.TabIndex = 1;
            this.ButtonAumentaCanal.Text = "+";
            this.ButtonAumentaCanal.UseVisualStyleBackColor = true;
            this.ButtonAumentaCanal.Click += new System.EventHandler(this.ButtonAumentaCanal_Click);
            // 
            // ButtonDiminuiCanal
            // 
            this.ButtonDiminuiCanal.Enabled = false;
            this.ButtonDiminuiCanal.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDiminuiCanal.Location = new System.Drawing.Point(6, 19);
            this.ButtonDiminuiCanal.Name = "ButtonDiminuiCanal";
            this.ButtonDiminuiCanal.Size = new System.Drawing.Size(54, 58);
            this.ButtonDiminuiCanal.TabIndex = 0;
            this.ButtonDiminuiCanal.Text = "-";
            this.ButtonDiminuiCanal.UseVisualStyleBackColor = true;
            this.ButtonDiminuiCanal.Click += new System.EventHandler(this.ButtonDiminuiCanal_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelVolume);
            this.groupBox2.Controls.Add(this.TrackBarVolume);
            this.groupBox2.Location = new System.Drawing.Point(192, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(183, 139);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Volume";
            // 
            // TrackBarVolume
            // 
            this.TrackBarVolume.Enabled = false;
            this.TrackBarVolume.Location = new System.Drawing.Point(6, 32);
            this.TrackBarVolume.Maximum = 100;
            this.TrackBarVolume.Name = "TrackBarVolume";
            this.TrackBarVolume.Size = new System.Drawing.Size(171, 45);
            this.TrackBarVolume.TabIndex = 0;
            this.TrackBarVolume.Scroll += new System.EventHandler(this.TrackBarVolume_Scroll);
            // 
            // labelVolume
            // 
            this.labelVolume.AutoSize = true;
            this.labelVolume.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVolume.Location = new System.Drawing.Point(73, 80);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(30, 25);
            this.labelVolume.TabIndex = 1;
            this.labelVolume.Text = "--";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(387, 261);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonOnOff);
            this.Controls.Add(this.labelStatus);
            this.Name = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button ButtonOnOff;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelCanal;
        private System.Windows.Forms.Button ButtonAumentaCanal;
        private System.Windows.Forms.Button ButtonDiminuiCanal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.TrackBar TrackBarVolume;
    }
}

