﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
    public partial class Form1 : Form
    {
        private Tv minhaTV;

        public Form1()
        {
            InitializeComponent();
            minhaTV = new Tv();

            labelStatus.Text = minhaTV.EnviaMensagem();
        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            if (!minhaTV.GetEstado())
            {
                minhaTV.LigaTv();
                labelStatus.Text = minhaTV.Mensagem;
                ButtonOnOff.Text = "OFF";
                ButtonAumentaCanal.Enabled = true;
                ButtonDiminuiCanal.Enabled = true;
                labelCanal.Text = minhaTV.Canal.ToString();
                TrackBarVolume.Enabled = true;
                labelVolume.Text = minhaTV.Volume.ToString();
                TrackBarVolume.Value = minhaTV.Volume;
            }
            else
            {
                minhaTV.DesligaTv();
                labelStatus.Text = minhaTV.Mensagem;
                ButtonOnOff.Text = "ON";
                ButtonAumentaCanal.Enabled = false;
                ButtonDiminuiCanal.Enabled = false;
                labelCanal.Text = "--";
                TrackBarVolume.Enabled = false;
                labelVolume.Text = "--";
            }

        }

        private void ButtonAumentaCanal_Click(object sender, EventArgs e)
        {
            minhaTV.Canal ++;
            labelCanal.Text = minhaTV.Canal.ToString();
        }

        private void ButtonDiminuiCanal_Click(object sender, EventArgs e)
        {
           minhaTV.Canal --;
           labelCanal.Text = minhaTV.Canal.ToString();
        }

        private void TrackBarVolume_Scroll(object sender, EventArgs e)
        {
            minhaTV.Volume = TrackBarVolume.Value;
            labelVolume.Text = minhaTV.Volume.ToString();
        }
    }
}
